# emmet-vim for Pearl

[emmet](https://emmet.io) for vim

## Details

- Plugin: https://github.com/mattn/emmet-vim
- Pearl: https://github.com/pearl-core/pearl
